const express = require('express')
const socketIo = require('socket.io')
const http = require('http')

const APP_CONFIG = require('./config')

const app = express()
const server = http.createServer(app)


const io = socketIo(server,{ 
    cors: {
      origin: 'http://localhost:3000'
    }
}) //in case server and client run on different urls


io.on('connection',(socket)=>{
    console.log('client connected: ',socket.id)
    
    socket.join('clock-room')
    
    socket.on('disconnect',(reason)=>{
      console.log(reason)
    })
})

app.get('/health', (req, res) => {
    return res.json({status: 'up'})
})

app.get('/', (req, res) => {
    res.sendFile(__dirname+'/index.html')
})

setInterval(()=>{
    io.to('clock-room').emit('time', new Date())
},1000)

server.listen(APP_CONFIG['PORT'], () => console.log(`listening on ${APP_CONFIG['PORT']}`))